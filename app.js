const express = require('express')
const path = require('path')
const app = express()
const Movie = require('./movie.js')
var cors = require('cors')


app.use(express.json()) //parse JSON body
app.use(express.urlencoded({ extended: true })) //parse x-www-form-urlencoded body
app.use(express.static('public'))
app.use(cors())

var movies = [];
var lastId = 0;

let mov0 = new Movie(0, 'Interstellar', 2014, 'USA', 'Christopher Nolan', 'Aventure, Drame, Sci-Fi', 'https://free4kwallpapers.com/uploads/wallpaper/interstellar-space-wallpaper-1024x768-wallpaper.jpg', 'Le film raconte les aventures d’un groupe d’explorateurs qui utilisent une faille récemment découverte dans l’espace-temps afin de repousser les limites humaines et partir à la conquête des distances astronomiques dans un voyage interstellaire.', 5);
let mov1 = new Movie(1, 'Intouchables', 2011, 'Français', 'Eric Toledano, Olivier Nakache', 'Drame', 'https://wallpapercave.com/wp/wp4245803.jpg', 'A la suite d’un accident de parapente, Philippe, riche aristocrate, engage comme aide à domicile Driss, un jeune de banlieue tout juste sorti de prison. Bref la personne la moins adaptée pour le job. Ensemble ils vont faire cohabiter Vivaldi et Earth Wind and Fire, le verbe et la vanne, les costumes et les bas de survêtement... Deux univers vont se télescoper, s’apprivoiser, pour donner naissance à une amitié aussi dingue, drôle et forte qu’inattendue, une relation unique qui fera des étincelles et qui les rendra... Intouchables.', 4);
let mov2 = new Movie(2, 'Fight Club', 1999, 'USA', 'David Fincher', 'Thriller, Drame', 'https://i.pinimg.com/originals/3d/a2/e1/3da2e1aed80c57e7d38e13e4ca4f596d.jpg', 'Le narrateur, sans identité précise, vit seul, travaille seul, dort seul, mange seul ses plateaux-repas pour une personne comme beaucoup d\'autres personnes seules qui connaissent la misère humaine, morale et sexuelle. C\'est pourquoi il va devenir membre du Fight club, un lieu clandestin ou il va pouvoir retrouver sa virilité, l\'échange et la communication. Ce club est dirigé par Tyler Durden, une sorte d\'anarchiste entre gourou et philosophe qui prêche l\'amour de son prochain.', 3);
let mov3 = new Movie(3, 'Shutter Island', 2010, 'USA', 'Martin Scorsese', 'Thriller', 'https://wallpapercave.com/wp/wp2073454.jpg', 'En 1954, le marshal Teddy Daniels et son coéquipier Chuck Aule sont envoyés enquêter sur l\'île de Shutter Island, dans un hôpital psychiatrique où sont internés de dangereux criminels. L\'une des patientes, Rachel Solando, a inexplicablement disparu. Comment la meurtrière a-t-elle pu sortir d\'une cellule fermée de l\'extérieur ? Le seul indice retrouvé dans la pièce est une feuille de papier sur laquelle on peut lire une suite de chiffres et de lettres sans signification apparente. Oeuvre cohérente d\'une malade, ou cryptogramme ?', 3);
let mov4 = new Movie(4, 'Le cinquième élément', 1997, 'Français', 'Luc Besson', 'Sci-fi', 'https://images8.alphacoders.com/668/thumb-1920-668261.jpg', 'Au XXIII siècle, dans un univers étrange et coloré, où tout espoir de survie est impossible sans la découverte du cinquième élément, un héros affronte le mal pour sauver l\'humanité.', 1);

movies.push(mov0);
movies.push(mov1);
movies.push(mov2);
movies.push(mov3);
movies.push(mov4);

//get every movies
app.get('/api/movies/all', (req, res, next) => {
    res.send(movies);
    console.log("Sent every movies");
})

//get movie from id
app.get('/api/movies/:id', (req, res, next) => {
    //get movie
    let mov = movies.find(m => m.id == req.params.id);

    //send it to client
    res.send(mov);
    console.log('Sent movie ID:'+mov.id);
})

//edit movie
app.post('/api/movies/:id', (req, res, next) => {
    //get movie
    let mov = movies.find(m => m.id == req.params.id);

    //remove movie
    index = movies.map(function(item) {
        return item.id;
    }).indexOf(mov.id);
    movies.splice(index, 1);

    //edit movie
    mov = req.body;

    //add edited movie
    movies.push(mov);
    console.log('Movie ID:'+mov.id+' has been edited');
})

//add new movie
app.post('/api/movies', (req, res, next) => {
    //set last id
    lastId++;

    //set new movie
    let m = req.body;
    let mov = new Movie(lastId, m.title, m.release, m.language, m.director, m.genre, m.image, m.synopsis, m.note);

    //add new movie
    movies.push(mov);

    console.log("Movie ID:"+mov.id+' has been added');
})

//delete movie
app.post('/api/movies/:id/delete', (req, res, next) => {
    //get movie
    let mov = movies.find(m => m.id == req.params.id);
    console.log(req.params.id);
    //delete movie
    index = movies.map(function(item) {
        return item.id;
    }).indexOf(mov.id);
    movies.splice(index, 1);

    console.log("Movie ID:"+mov.id+' has been deleted');
})

//handling errors
app.use((err, req, res, next) => {
    console.error("--------------------ERROR--------------------");
    console.error(err.stack);
    res.status(500).send('error 500');
    console.error("---------------------------------------------");

})

app.listen(3000, () => console.log('Server is running on port 3000'));
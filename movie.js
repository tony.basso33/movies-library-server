class Movie 
{
    id = 0;
    title = "";
    release = "";
    language = "";
    director = "";
    genre = "";
    image = "";
    synopsis= "";
    note = 0;
 
    constructor(id, title, release = "",  language = "", director ="", genre = "", image="", synopsis= "", note=0) 
    {
        this.id = id;
        this.title = title;
        this.release = release;
        this.language = language;
        this.director = director;
        this.genre = genre;
        this.image = image;
        this.synopsis = synopsis;
        this.note = note;
    }
      
}

module.exports = Movie